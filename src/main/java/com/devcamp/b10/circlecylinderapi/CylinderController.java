package com.devcamp.b10.circlecylinderapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b10.circlecylinderapi.models.Cylinder;

@RestController
@RequestMapping("/cylinder-volume")
public class CylinderController {
    @CrossOrigin
    @GetMapping

    public double getCylinderVolume(@RequestParam double radius, @RequestParam double height){
        Cylinder cylinder = new Cylinder(radius, height);

        return cylinder.getVolume(height, height);
    }
}
