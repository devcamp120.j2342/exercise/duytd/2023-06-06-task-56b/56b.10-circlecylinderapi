package com.devcamp.b10.circlecylinderapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CircleCylinderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircleCylinderApiApplication.class, args);
	}

}
